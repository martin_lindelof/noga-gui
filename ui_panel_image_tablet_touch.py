import bpy


class TabletTouchPanel(bpy.types.Panel):
    """A tiny helper touch ux for Tablets when drawing"""
    bl_label = "Tablet Touch Panel"
    bl_idname = "OBJECT_PT_hello"
    bl_space_type = 'IMAGE_EDITOR'
    bl_region_type = 'TOOLS'
    bl_context = "object"

    def draw(self, context):
        layout = self.layout

        #row = layout.row()
        #row.operator("image.view_all")
        #row = layout.row()
        #row.operator("image.view_zoom_in")
        #row = layout.row()
        #row.operator("image.view_pan")

        col = layout.column(align=True)
        col.scale_y = 2.0
        col.operator("image.view_pan",text="Move / Pan")
        
        col = layout.column(align=True)
        col.scale_y = 2.0
        col.operator("image.view_zoom_in", text="Zoom In +")
        
        col = layout.column(align=True)
        col.scale_y = 2.0
        col.operator("image.view_zoom_out",text="Zoom Out -")
        
        col = layout.column(align=True)
        col.scale_y = 2.0
        col.operator("image.view_all")



def register():
    bpy.utils.register_class(TabletTouchPanel)


def unregister():
    bpy.utils.unregister_class(TabletTouchPanel)


if __name__ == "__main__":
    register()
