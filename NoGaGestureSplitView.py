bl_info = {
    "name": "NOGA Split View Gesture Script",
    "description": "Splits current view by cursor direction gesture",
    "author": "Martin Lindelöf",
    "version": (0, 1),
    "blender": (2, 70, 0),
    "location": "View > Area (any)",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"
                "Scripts/My_Script",
    "category": "System"}

import bpy
from bpy.props import IntProperty, FloatProperty


class NoGaGestureSplitView(bpy.types.Operator):
    """Split the view area based on cursor position and direciton gesture"""
    bl_idname = "screen.noga_gesture_split_view"
    bl_label = "Split view area with a direction gesture"

    first_mouse_x = IntProperty()
    first_mouse_y = IntProperty()
    delta_mouse_x = IntProperty()
    delta_mouse_y = IntProperty()
    factor = FloatProperty()

    def modal(self, context, event):
        if event.type == 'MOUSEMOVE':
            delta_mouse_x = self.first_mouse_x - event.mouse_x
            delta_mouse_y = self.first_mouse_y - event.mouse_y


            if delta_mouse_x > 50:#left
                self.factor = 1 - (context.region.height-event.mouse_region_y)/context.region.height
                bpy.ops.screen.area_split(direction='HORIZONTAL',factor=self.factor)
                return {'FINISHED'}
            elif delta_mouse_x < -50:#right
                self.factor = 1 - (context.region.height-event.mouse_region_y)/context.region.height
                bpy.ops.screen.area_split(direction='HORIZONTAL',factor=self.factor)
                return {'FINISHED'}
            if delta_mouse_y > 50:#up
                self.factor = 1 - (context.region.width-event.mouse_region_x)/context.region.width
                bpy.ops.screen.area_split(direction='VERTICAL',factor=self.factor)
                return {'FINISHED'}
            elif delta_mouse_y < -50:#down
                self.factor = 1 - (context.region.width-event.mouse_region_x)/context.region.width
                bpy.ops.screen.area_split(direction='VERTICAL',factor=self.factor)
                return {'FINISHED'}


        #elif event.type == 'LEFTMOUSE':
        #    return {'FINISHED'}

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            context.object.location.x = self.first_value
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        context.window_manager.modal_handler_add(self)
        self.first_mouse_x = event.mouse_x
        self.first_mouse_y = event.mouse_y
        self.delta_mouse_x = 0
        self.delta_mouse_y = 0
        self.factor = 0.5
        return {'RUNNING_MODAL'}


def register():
    bpy.utils.register_class(NoGaGestureSplitView)


def unregister():
    bpy.utils.unregister_class(NoGaGestureSplitView)


if __name__ == "__main__":
    register()

