bl_info = {
    "name": "NOGA Change View Gesture Script",
    "description": "Changes current view by cursor direction gesture",
    "author": "Martin Lindelöf",
    "version": (0, 1),
    "blender": (2, 70, 0),
    "location": "View > Area (any)",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"
                "Scripts/My_Script",
    "category": "System"}

import bpy
from bpy.props import IntProperty, FloatProperty


class NoGaGestureChangeView(bpy.types.Operator):
    """Change the view type with a direction gesture"""
    bl_idname = "screen.noga_gesture_change_view"
    bl_label = "Change view with a direction gesture"

    first_mouse_x = IntProperty()
    first_mouse_y = IntProperty()
    delta_mouse_x = IntProperty()
    delta_mouse_y = IntProperty()
    first_value = FloatProperty()

    GESTURE_UP = 'VIEW_3D'
    GESTURE_DOWN = 'IMAGE_EDITOR'
    GESTURE_RIGHT = 'GRAPH_EDITOR'
    GESTURE_LEFT = 'NODE_EDITOR'

    def modal(self, context, event):
        if event.type == 'MOUSEMOVE':
            delta_mouse_x = self.first_mouse_x - event.mouse_x
            delta_mouse_y = self.first_mouse_y - event.mouse_y

            if delta_mouse_x > 50:
                bpy.context.area.type = self.GESTURE_LEFT
                return {'FINISHED'}
            elif delta_mouse_x < -50:
                bpy.context.area.type = self.GESTURE_RIGHT
                return {'FINISHED'}
            if delta_mouse_y > 50:
                bpy.context.area.type = self.GESTURE_DOWN
                return {'FINISHED'}
            elif delta_mouse_y < -50:
                bpy.context.area.type = self.GESTURE_UP
                return {'FINISHED'}


        #elif event.type == 'LEFTMOUSE':
        #    return {'FINISHED'}

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            context.object.location.x = self.first_value
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        context.window_manager.modal_handler_add(self)
        self.first_mouse_x = event.mouse_x
        self.first_mouse_y = event.mouse_y
        self.delta_mouse_x = 0
        #self.delta_mouse_y = 0
        return {'RUNNING_MODAL'}


def register():
    bpy.utils.register_class(NoGaGestureChangeView)


def unregister():
    bpy.utils.unregister_class(NoGaGestureChangeView)


if __name__ == "__main__":
    register()
