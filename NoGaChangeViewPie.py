bl_info = {
    "name": "NoGa Change View Pie",
    "description": "Change View",
    "author": "Martin Lindelöf",
    "version": (1, 0),
    "blender": (2, 71, 4),
    "location": "View > All views",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"
                "Scripts/My_Script",
    "category": "UI"}

import bpy
from bpy.types import Menu
from bpy.props import IntProperty, FloatProperty, BoolProperty
import bmesh
from mathutils import *
import math

# spawn an edit mode selection pie (run while object is in edit mode to get a valid output)


class NOGA_VIEW_ChangeViewPie(Menu):
    # label is displayed at the center of the pie menu.
    bl_label = "Change View"
    bl_idname = "screen.noga_change_view_pie"

    def draw(self, context):
        layout = self.layout

        pie = layout.menu_pie()

        #4 - LEFT
        pie.operator("object.view_menu", text="Node Editor", icon= 'NODETREE').variable="NODE_EDITOR"
        #6 - RIGHT
        pie.operator("object.view_menu", text="Image Editor", icon= 'IMAGE_COL').variable="IMAGE_EDITOR"
        #2 - BOTTOM
        box.operator("object.view_menu", text="Graph Editor", icon= 'IPO').variable="GRAPH_EDITOR"
        #8 - TOP
        pie.operator("object.view_menu", text="VIEW 3D", icon= 'VIEW3D').variable="VIEW_3D"
        #7 - TOP - LEFT
        #box.operator("object.view_menu", text="Text Editor", icon= 'FILE_TEXT').variable="TEXT_EDITOR"
        #box.operator("object.view_menu", text="Outliner", icon= 'OOPS').variable="OUTLINER"
        #box.operator("object.view_menu", text="Properties", icon= 'BUTS').variable="PROPERTIES"
        #box.operator("object.view_menu", text="User Preferences", icon= 'PREFERENCES').variable="USER_PREFERENCES"
        #9 - TOP - RIGHT
        #box.operator("object.view_menu", text="Dope Sheet", icon= 'ACTION').variable="DOPESHEET_EDITOR"
        #box.operator("object.view_menu", text="Timeline", icon= 'TIME').variable="TIMELINE"


def register():
    bpy.utils.register_class(NOGA_VIEW_ChangeViewPie)


def unregister():
    bpy.utils.unregister_class(NOGA_VIEW_ChangeViewPie)


if __name__ == "__main__":
    register()


