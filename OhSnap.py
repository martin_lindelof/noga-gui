bl_info = {
    "name": "Oh Snap!",
    "description": "Easy snaps",
    "author": "Martin Lindelöf",
    "version": (1, 0),
    "blender": (2, 71, 4),
    "location": "View > 3d view",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"
                "Scripts/My_Script",
    "category": "UI"}

import bpy
from bpy.types import Menu

# spawn an edit mode selection pie (run while object is in edit mode to get a valid output)


class VIEW3D_PIE_oh_snap(Menu):
    # label is displayed at the center of the pie menu.
    bl_label = "Oh Snap"
    bl_idname = "view3d.oh_snap"

    def draw(self, context):
        layout = self.layout

        pie = layout.menu_pie()
        # operator_enum will just spread all available options
        # for the type enum of the operator on the pie
        # pie.operator_enum("mesh.select_mode", "type")
        # pie.snap_selected_to_cursor(use_offset=False)
        # pie.snap_selected_to_cursor()
        #pie.operator("view3d.snap_selected_to_grid")
        pie.operator("view3d.snap_selected_to_cursor")
        #pie.operator("view3d.snap_selected_to_cursor",text="True")
        pie.operator("view3d.snap_cursor_to_selected")
        #pie.operator("view3d.snap_cursor_to_center")
        #pie.operator("view3d.snap_cursor_to_grid")
        #pie.operator("view3d.snap_cursor_to_active")


def register():
    bpy.utils.register_class(VIEW3D_PIE_oh_snap)


def unregister():
    bpy.utils.unregister_class(VIEW3D_PIE_oh_snap)


if __name__ == "__main__":
    register()


